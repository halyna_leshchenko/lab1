<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<style>
    pre {
        font-family: "Times New Roman", sans-serif;
        margin:5px;
        font-size: 18px;
    }
    table {
        border-collapse: collapse;
    }
    td {
        width: 50px;
        height: 50px;
    }
    .container {
        background-color: black;
        width: 600px;
        height: 600px;
        position: relative;
        overflow: hidden;
    }
    .red_square {
        position: absolute;
        background-color: red;
    }
</style>
    <title>Document</title>
</head>
<body>
<?php
echo "<span style='color: blue; font-size: 20px'>task_2</span>";
echo "<br>";
echo "<pre>Полину в мріях в купель океану,</pre>";
echo "<pre>Відчую <b>шовковистість</b> глибини,</pre>";
echo "<pre> Чарівні мушлі з дна собі дістану,</pre>";
echo "<pre>   Щоб <b><em>взимку</em></b></pre>";
echo "<pre>          <u>тішили</u></pre>";
echo "<pre>                  мене</pre>";
echo "<pre>                       вони…</pre>";
echo "<br>";
?>
<?php
echo "<span style='color: blue; font-size: 20px'>task_3</span>";
$ua = 1500;
$kurs = 37.65;
$usd = $ua / $kurs;
echo "<p>{$ua} грн. можна обміняти на " . round($usd, 2) . " долар</p>";
echo "<br>";
?>
<?php
echo "<span style='color: blue; font-size: 20px'>task_4</span>";
$month = 7;

// Визначаємо сезон за номером місяця
if ($month >= 3 && $month <= 5) {
    $season = "весна";
} elseif ($month >= 6 && $month <= 8) {
    $season = "літо";
} elseif ($month >= 9 && $month <= 11) {
    $season = "осінь";
} else {
    $season = "зима";
}

echo "<p>Місяць {$month} це - {$season}</p>";
echo "<br>";
?>
<?php
echo "<span style='color: blue; font-size: 20px'>task_5</span>";
$char = 'а';

switch (strtolower($char)) {
    case 'а':
    case 'о':
    case 'у':
    case 'е':
    case 'и':
    case 'і':
        $result = "голосна";
        break;
    default:
        if (ctype_alpha($char)) {
            $result = "приголосна";
        } else {
            $result = "не буква";
        }
}

echo "<p>'{$char}' є {$result}</p>";
echo "<br>";
?>
<?php
echo "<span style='color: blue; font-size: 20px'>task_6</span>";
$num = mt_rand(100, 999);
$sum = array_sum(str_split($num));

$revers = strrev($num);

$digits = str_split($num);
rsort($digits);
$max = implode('', $digits);

echo "<p>Випадкове тризначне число: {$num}</p>";
echo "<p>1. Сума його цифр: {$sum}</p>";
echo "<p>2. Число, отримане виписуванням в зворотному порядку цифр: {$revers}</p>";
echo "<p>3. Найбільше число, яке можна отримати: {$max}</p>";
echo "<br>";
?>
<?php
echo "<span style='color: blue; font-size: 20px'>task_7</span>";
echo "<br>";
echo "<span style='color: blue; font-size: 20px'>1)</span>";
echo "<br>";
function table($rows, $columns) {
    echo '<table>';
    for ($i = 0; $i < $rows; $i++) {
        echo '<tr>';
        for ($j = 0; $j < $columns; $j++) {
            $color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
            echo "<td style='background-color: $color;'></td>";
        }
        echo '</tr>';
    }
    echo '</table>';
}

table(5, 5);
echo "<br>";
?>
<div class="container">
<?php
echo "<span style='color: blue; font-size: 20px'>2)</span>";
function RedSquares($count) {
    for ($i = 0; $i < $count; $i++) {
        $size = mt_rand(20, 100);
        $left = mt_rand(5, 600);
        $top = mt_rand(5, 600);
        echo "<div class='red_square' style='width: {$size}px; height: {$size}px; left: {$left}px; top: {$top}px;'></div>";
    }
}

RedSquares(10);
?>
</div>
</body>
</html>